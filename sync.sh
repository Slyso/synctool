#!/bin/bash

basePathIt=/mnt/skripteserver/Informatik/Fachbereich/
basePathScience=/mnt/skripteserver/Mathematik_Naturwissenschaften/

targetDir=~/Documents/Schule/Scripts

itPaths=(
  Cybersecurity_Foundations/
  Algorithmen_und_Datenstrukturen_1/
  Betriebssysteme_2/
  Web_Engineering_+_Design_1/
  Experimentieren_und_Evaluieren_fuer_Informatik/
)

sciencePaths=(
  Analysis_2_fuer_Informatiker/
)

paths=()

if [ ! -d "$basePathIt" ] || [ ! -d "$basePathScience" ] ; then
  echo "Source directories not found! Make sure the server is mounted"
  exit
fi

if [ ! -d "$targetDir" ] ; then
  echo "Target directory not found: $targetDir"
  exit
fi

for path in ${itPaths[*]}
do
  paths+=($basePathIt$path)
done

for path in ${sciencePaths[*]}
do
  paths+=($basePathScience$path)
done

for path in ${paths[*]}
do
  if [ ! -d "$path" ] ; then
    echo "Error: path $path not found!"
    continue;
  fi

  for dir in $(basename $path*/) ; do
    target="$targetDir/$dir"
    src="$path$dir"

    if [ ! -d "$target" ] ; then mkdir $target; fi

    echo "$src ===> $target"
    pushd $src > /dev/null
    rsync -au --delete . $target
    popd > /dev/null
  done
done

